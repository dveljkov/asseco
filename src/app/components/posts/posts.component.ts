import { Component, OnInit } from '@angular/core';
import { PostsService } from './posts.service';
import { Post } from './model/post';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
  posts: Post[];

  constructor(private postsService: PostsService) { }

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts() {
    this.postsService.getPosts().subscribe(res => {
      this.posts = res;
    },
    resCategoryError => {
      this.setErroeMessage(resCategoryError);
    })
  }

  setErroeMessage(error: any) {
    console.log('controller error ' + JSON.stringify(error));
  }
}
