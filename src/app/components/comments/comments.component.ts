import { Component, OnInit, Input } from '@angular/core';
import { CommentsService } from './comments.service';
import { Comment } from './model/comment';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @Input() postId: number;
  comments: Comment[];

  constructor(private commentsService: CommentsService) { }

  ngOnInit() {
    if(this.postId) {
      this.loadComments(this.postId);
    }
  }
  
  loadComments(postId: number) {
    this.commentsService.getComments(postId).subscribe(res => {
      this.comments = res;
    },
    resCategoryError => {
      this.setErroeMessage(resCategoryError);
    })
  }

  setErroeMessage(error: any) {
    console.log('controller error ' + JSON.stringify(error));
  }
}
