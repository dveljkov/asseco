import { Component, OnInit } from '@angular/core';
import { AlbumsService } from './albums.service';
import { Album } from './model/album';
import { Router } from '@angular/router';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.scss']
})
export class AlbumsComponent implements OnInit {

  albums: Album[];

  constructor(private albumsService: AlbumsService, private router: Router) { }

  ngOnInit() {
    this.loadAlbums();
  }

  loadAlbums() {
    this.albumsService.getAlbums().subscribe(res => {
      this.albums = res;
    },
    resCategoryError => {
      this.setErroeMessage(resCategoryError);
    });
  }

  navigateToAlbum(album: Album) {
    this.router.navigateByUrl('/photos/'+album.id);
  }

  setErroeMessage(error: any) {
    console.log('controller error ' + JSON.stringify(error));
  }
}
