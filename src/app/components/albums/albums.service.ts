import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { config } from '../../config';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {

  constructor(private http: HttpClient) { }

  getAlbums() {
    return this.http.get<any>(`${config.apiUrl}/albums`).pipe(
      map (res => res),
      catchError((err: Response) => {
        return this._errorHandler(err);
      })
    )
  }

  _errorHandler(err: Response) {
    return throwError(err);
  }
}
