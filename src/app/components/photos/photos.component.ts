import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotosService } from './photos.service';
import { Photo } from './model/photo';
import { Location } from '@angular/common';

@Component({
  selector: 'app-photos',
  templateUrl: './photos.component.html',
  styleUrls: ['./photos.component.scss']
})
export class PhotosComponent implements OnInit {
  photos: Photo[];
  photoToDisplay: Photo;

  constructor(private route: ActivatedRoute, private photosService: PhotosService, private location: Location) { }

  ngOnInit() {
    this.loadPhotos();
  }

  loadPhotos() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.photosService.getPhotosForAlbum(id).subscribe(res => {
      this.photos = res;
    },
    resCategoryError => {
      this.setErroeMessage(resCategoryError);
    })
  }

  showPhoto(photo: Photo) {
    this.photoToDisplay = photo;
    document.getElementById('modalButton').click();
  }

  goBack() {
    this.location.back();
  }

  setErroeMessage(error: any) {
    console.log('controller error ' + JSON.stringify(error));
  }
}
