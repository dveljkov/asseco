import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AlbumsComponent } from './components/albums/albums.component';
import { AlbumsService } from './components/albums/albums.service';
import { CommentsComponent } from './components/comments/comments.component';
import { CommentsService } from './components/comments/comments.service';
import { PostsComponent } from './components/posts/posts.component';
import { PostsService } from './components/posts/posts.service';
import { PhotosComponent } from './components/photos/photos.component';
import { PhotosService } from './components/photos/photos.service';

@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    CommentsComponent,
    PostsComponent,
    PhotosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [AlbumsService, CommentsService, PostsService, PhotosService],
  bootstrap: [AppComponent]
})
export class AppModule { }
